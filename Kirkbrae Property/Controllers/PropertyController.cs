﻿

using System;
﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Kirkbrae_Property.Models;

namespace Kirkbrae_Property.Controllers
{
    public partial class PropertyController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public ActionResult Index()
        {
            //var properties = db.Properties.ToList();
            //if (properties == null)
            //{
                var propertiesList = new List<Property>();
                var property = new Property()
                {
                    Name = "Applewood",
                    Location = "Fulmar Brae, Edinburgh, EH11",
                    Listed = DateTime.Now.Date,
                    Price = 1000
                };
                propertiesList.Add(property);
                var property2 = new Property()
                {
                    Name = "Cabin",
                    Location = "West Pilton Rise, Edinburgh, EH11",
                    Listed = DateTime.Now.Date,
                    Price = 1200
                };
                propertiesList.Add(property2);
                var property3 = new Property()
                {
                    Name = "Deluxe Mansion",
                    Location = "Simpson View, Edinburgh, EH11",
                    Listed = DateTime.Now.Date,
                    Price = 1300
                };
                propertiesList.Add(property3);
                return View(propertiesList);
            //}
            //return View(db.Properties.ToList());
        }

        public ActionResult Details(int id = 0)
        {
            Property property = db.Properties.Find(id);
            if (property == null)
            {
                return HttpNotFound();
            }
            return View(property);
        }

        public ActionResult Create()
        {
            return View();
        }

       [HttpPost]
       [ValidateAntiForgeryToken]
        public ActionResult Create(Property property)
        {
            if (ModelState.IsValid)
            {
                db.Properties.Add(property);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(property);
        }

        public ActionResult Edit(int id = 0)
        {
            Property property = db.Properties.Find(id);
            if (property == null)
            {
                return HttpNotFound();
            }
            return View(property);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Property property)
        {
            if (ModelState.IsValid)
            {
                db.Entry(property).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(property);
        }

        public ActionResult Delete(int id = 0)
        {
            Property property = db.Properties.Find(id);
            if (property == null)
            {
                return HttpNotFound();
            }

            return View(property);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            db.Database.ExecuteSqlCommand("DELETE FROM Properties WHERE ID = {0}", id.ToString());
            db.SaveChanges();

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}

