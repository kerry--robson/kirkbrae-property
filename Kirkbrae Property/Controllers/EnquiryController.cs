﻿

using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Kirkbrae_Property.Models;

namespace Kirkbrae_Property.Controllers
{
    public partial class EnquiryController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public ActionResult Index()
        {
            //var enquiries = db.Enquiries.ToList();
            //if (enquiries == null)
            //{
                var enquiriesList = new List<Enquiry>();
                var enquiry = new Enquiry()
                {
                    Id = 1,
                    UserId = 1,
                    Message = "Is this property still available?",
                    Status = "Pending review",
                    LastUpdated = DateTime.Now
                };
                enquiriesList.Add(enquiry);
                return View(enquiriesList);
            //}
            //return View(db.Enquiries.ToList());
        }

        public ActionResult Details(int id = 0)
        {
            Enquiry enquiry = db.Enquiries.Find(id);
            if (enquiry == null)
            {
                return HttpNotFound();
            }
            return View(enquiry);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Enquiry enquiry)
        {
            if (ModelState.IsValid)
            {
                var newEnquiry = new Enquiry
                {
                    UserId = 1,
                    Message = enquiry.Message,
                    Status = "Awaiting response",
                    LastUpdated = DateTime.UtcNow
                };

                db.Enquiries.Add(newEnquiry);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(enquiry);
        }

        public ActionResult Edit(int id = 0)
        {
            Enquiry enquiry = db.Enquiries.Find(id);
            if (enquiry == null)
            {
                return HttpNotFound();
            }
            return View(enquiry);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Enquiry enquiry)
        {
            if (ModelState.IsValid)
            {
                db.Entry(enquiry).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(enquiry);
        }

        public ActionResult Delete(int id = 0)
        {
            Enquiry enquiry = db.Enquiries.Find(id);
            if (enquiry == null)
            {
                return HttpNotFound();
            }

            return View(enquiry);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            db.Database.ExecuteSqlCommand("DELETE FROM Enquiries WHERE ID = {0}", id.ToString());
            db.SaveChanges();

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}