﻿using System.Web;
using System.Web.Mvc;

namespace Kirkbrae_Property
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
