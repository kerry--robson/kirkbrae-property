﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kirkbrae_Property.Models
{
    public class Photo
    {
        public int Id { get; set; }
        public string FileName { get; set; }
        public byte[] Image { get; set; }
        public bool DisplayItem { get; set; }
    }
}