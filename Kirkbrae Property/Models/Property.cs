﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kirkbrae_Property.Models
{
    public class Property
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }
        public DateTime Listed { get; set; }
        public int Price { get; set; }
        public string Description { get; set; }
        public Room Room { get; set; }
    }
}