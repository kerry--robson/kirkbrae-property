﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kirkbrae_Property.Models
{
    public class Room
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public double? Measurements { get; set; }
        public string Description { get; set; }
    }
}