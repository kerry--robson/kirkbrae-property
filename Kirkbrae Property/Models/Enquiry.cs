﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kirkbrae_Property.Models
{
    public class Enquiry
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Message { get; set; }
        public string Status { get; set; }
        public DateTime? LastUpdated { get; set; }
    }
}