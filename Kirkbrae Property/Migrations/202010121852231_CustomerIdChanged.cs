﻿namespace Kirkbrae_Property.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CustomerIdChanged : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Enquiries", "UserId", c => c.Int(nullable: false));
            DropColumn("dbo.Enquiries", "CustomerId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Enquiries", "CustomerId", c => c.Int(nullable: false));
            DropColumn("dbo.Enquiries", "UserId");
        }
    }
}
