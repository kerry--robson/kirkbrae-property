﻿namespace Kirkbrae_Property.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CustomerIdAdded : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Enquiries", "CustomerId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Enquiries", "CustomerId");
        }
    }
}
