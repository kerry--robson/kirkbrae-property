﻿namespace Kirkbrae_Property.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EnquiryStatusAndLastUpdatedAdded : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Enquiries", "Status", c => c.String());
            AddColumn("dbo.Enquiries", "LastUpdated", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Enquiries", "LastUpdated");
            DropColumn("dbo.Enquiries", "Status");
        }
    }
}
