﻿namespace Kirkbrae_Property.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateUsers : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO dbo.Users (FirstName, LastName, EmailAddress, PhoneNumber) VALUES ('Kerry', 'Robson', 'kerry-robson@email.com', 07970559637)");
            Sql("INSERT INTO dbo.Users (FirstName, LastName, EmailAddress, PhoneNumber) VALUES ('Fraser', 'Martin', 'martin123@outlook.com', 07970559745)");
            Sql("INSERT INTO dbo.Users (FirstName, LastName, EmailAddress, PhoneNumber) VALUES ('Alex', 'Moore', 'alexwilson@gmail.com', 07948557456)");
            Sql("INSERT INTO dbo.Users (FirstName, LastName, EmailAddress, PhoneNumber) VALUES ('Aimee', 'Jones', 'jones345@hotmail.com', 07948554856)");
            Sql("INSERT INTO dbo.Users (FirstName, LastName, EmailAddress, PhoneNumber) VALUES ('Mary', 'Smith', 'smithfamily@hotmail.co.uk', 07858660496)");
            Sql("INSERT INTO dbo.Users (FirstName, LastName, EmailAddress, PhoneNumber) VALUES ('David', 'Copper', 'copperd@hotmail.com', 07858660006)");
        }
        
        public override void Down()
        {
        }
    }
}
