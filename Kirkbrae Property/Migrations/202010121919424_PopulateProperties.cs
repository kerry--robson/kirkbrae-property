﻿namespace Kirkbrae_Property.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateProperties : DbMigration
    {
        public override void Up()
        {
            string updated = DateTime.Now.ToUniversalTime().ToString("yyyy-MM-dd hh:mm:ss.sss");

            Sql("INSERT INTO dbo.Properties (Name, Location, Listed, Price) VALUES ('Applewood', 'Fulmar Brae, Edinburgh, EH11', '" + updated + "', 1000.00)");
            Sql("INSERT INTO dbo.Properties (Name, Location, Listed, Price) VALUES ('Cabin', 'West Pilton Rise, Edinburgh, EH11', '" + updated + "', 677.00)");
            Sql("INSERT INTO dbo.Properties (Name, Location, Listed, Price) VALUES ('Cottage House', 'Fulmar Brae, Edinburgh, EH11', '" + updated + "', 899.00)");
            Sql("INSERT INTO dbo.Properties (Name, Location, Listed, Price) VALUES ('Deluxe Mansion', 'Fulmar Brae, Edinburgh, EH11', '" + updated + "', 788.00)");
        }

        public override void Down()
        {
        }
    }
}
